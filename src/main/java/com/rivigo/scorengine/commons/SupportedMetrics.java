package com.rivigo.scorengine.commons;

public class SupportedMetrics {
    public enum prometheusMetrics {
        system_load_average_1m,
        logback_events_total,
        data_source_active_connections,
        http_server_requests_duration_seconds,
        http_server_requests_duration_seconds_count,
        jvm_memory,
        jvm_blocked_thread,
        jvm_active_thread
    }
}
