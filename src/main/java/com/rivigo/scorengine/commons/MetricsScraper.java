package com.rivigo.scorengine.commons;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rivigo.scorengine.exporter.Exporter;
import com.rivigo.scorengine.exporter.PrometheusExporter;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import io.prometheus.client.SimpleCollector;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class MetricsScraper {
    private ConfigReader configReader;
    private ScheduledExecutorService scheduler;
    @Getter
    private ConcurrentHashMap<String, Gauge> gaugeMap;
    @Getter
    private ConcurrentHashMap<String , Exporter> exporterMap;
    @Getter
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private Counter cleanedMetrics;
    private static final long metricsTtl =  80000;

    public MetricsScraper(ConfigReader configReader, HttpClient httpClient, ScheduledExecutorService scheduler) {
        this.configReader = configReader;
        this.scheduler = scheduler;

        gaugeMap = new ConcurrentHashMap<>();
        exporterMap = new ConcurrentHashMap<>();

        cleanedMetrics = Counter.build().name("cleanedMetrics").help("cleanedMetrics").register();

        Map<String, TargetServiceMetadata.TargetMetadata> targetMetadata =
                configReader.getTargetServiceMetadata().getTarget();
        TargetServiceMetadata targetServiceMetadata = configReader.getTargetServiceMetadata();
        for(Map.Entry<String, TargetServiceMetadata.TargetMetadata> entry : targetMetadata.entrySet()) {
            log.debug("scheduling for service - {} ", entry.getKey() );
            log.trace("config value {} ", gson.toJson(entry.getValue()));
            scheduler.scheduleWithFixedDelay(() -> scheduleTargets(httpClient, entry.getKey(), entry.getValue())
            , entry.getValue().getInitialdelay(),
                    targetServiceMetadata.getGlobal().getInterval(), TimeUnit.SECONDS );
            scheduler.scheduleWithFixedDelay(() -> cleaner(), 500, 60 , TimeUnit.SECONDS );

        }
    }

    public void scheduleTargets(HttpClient httpClient, String serviceName,
                                TargetServiceMetadata.TargetMetadata targetMetadata) {
        List<String> ips = targetMetadata.getIps();
        if (CollectionUtils.isNotEmpty(ips)) {
            for (String ip : ips) {
                try {
                    String name = getName(serviceName, ip);
                    log.info("scraping for ip : {} ", ip);
                    HttpResponse httpResponse = httpClient.execute(formHttpGetRequest(ip));
                    Exporter exporter = null;
                    if (targetMetadata.getFormat().equalsIgnoreCase(SupportedFormat.prometheus.toString())) {
                        exporter = new PrometheusExporter(httpResponse, targetMetadata);
                    }
                    exporterMap.put(name, exporter);
                    Gauge gauge = gaugeMap.get(name);
                    if (gauge == null) {
                        gauge = Gauge.build().name(name).help("score").register();
                        gaugeMap.put(name, gauge);
                    }
                    double totalScore = calculateTotalScore(exporter);
                    //totalScore <=0 ? 0 : totalScore
                    gauge.set(totalScore);
                } catch (Exception e) {
                    log.error("error while scraping for service {} and ip {}", serviceName, ip, e);
                }
            }
        } else {
            log.warn("not find any ips for service {} ", serviceName);
        }
    }

    public HttpGet formHttpGetRequest(String ip) {
        return new HttpGet(ip);
    }

    public double calculateTotalScore(Exporter e) {
//        log.debug("calculating overall score from score\n {}", gson.toJson(e, PrometheusExporter.class));
        return ((.1 * e.getLoggingScore()) +
                (.2 * e.getdbsScore()) +
                (.1 * e.getResponseTimeScore()) +
                (.1 * e.getResponseCodeScore()) +
                (.1 * e.getMemoryScore()) +
                (.2 * e.getThreadScore()) + (.1 * e.getCpuScore())) /(.9);

    }

    public void cleaner() {
        log.info("cleaning old metrices");
        for(Map.Entry<String, Exporter> entry : exporterMap.entrySet()) {
            try {
                if (System.currentTimeMillis() - entry.getValue().getLastUpdated() > metricsTtl) {
                    if (gaugeMap.get(entry.getKey()) != null) {
                        CollectorRegistry.defaultRegistry.unregister(gaugeMap.get(entry.getKey()));
                        gaugeMap.remove(entry.getKey());
                        cleanedMetrics.inc();
                    }
                    exporterMap.remove(entry.getKey());
                }
            } catch (RuntimeException e){
                log.error("exception while unregistering", e);
            }
        }
    }

    public String getName(String serviceName, String ip ) {
        String name = serviceName + "_" +
                ip.split("/")[2].replaceAll("-", "_").replaceAll("\\.", "_");
        return name;

    }

}
