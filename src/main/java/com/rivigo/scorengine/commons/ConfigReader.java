package com.rivigo.scorengine.commons;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.Configuration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Getter
@Setter
@Slf4j
public class ConfigReader {
    private TargetServiceMetadata targetServiceMetadata;
    private Configuration scoreConfig;

    public ConfigReader() throws IOException {
        String filePath = System.getProperty("config-file");
        File configFile = null;
        InputStream is = null;
        if(filePath == null){
            log.warn("config file not specified. looking into default location");
            //try to load from classpath
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            is = cl.getResourceAsStream("targetService.yaml");
            if(is == null) {
                configFile = new File("/tmp/targetService.yaml");
                if(!configFile.exists()) {
                    throw new RuntimeException("can not find config file");
                }
            }
        }
        else {
            configFile = new File(filePath);
        }
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        if(is != null) {
            log.info("using classpath file targetService.yaml");
            targetServiceMetadata = mapper.readValue(is, TargetServiceMetadata.class);
        }
        else {
            log.info("using file {} ", configFile.getAbsolutePath());
            targetServiceMetadata = mapper.readValue(configFile, TargetServiceMetadata.class);

        }
        //empty config file for now
        scoreConfig = new BaseConfiguration();
    }


}
