package com.rivigo.scorengine.commons;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Getter
@Setter
public class TargetServiceMetadata {
    private GlobalConfig global;
    private Map<String, TargetMetadata> target;


    @Data
    public static class GlobalConfig {
        private int schedulerThread;
        private int interval;
        private List<String> consulIps;
        private List<String> zookeeperIps;
        private int discoveryTtl;

    }

    @Getter
    public static class TargetMetadata{
        int discovery;
        String platform;
        String name;
        String metricsUri;
        List<String> ips;
        String format;
        int initialdelay;
        int maxErrorLog;
        int minErrorLog;
        int maxWarnLog;
        int minWarnLog;
        int maxDbaConnection;
        int minDbaConnection;
        int maxSysLoad;
        int minSysLoad;
        int maxreqLatency;
        int minReqLatency;
        int maxmemory;
        int minMemory;
        int maxBlockedThread;
        int minBlockedThread;
        int maxNonOk;
        protected ReentrantReadWriteLock rwlock = new ReentrantReadWriteLock();

        public void setIps(List<String> ipslist) {
            rwlock.writeLock().lock();
            this.ips = ipslist;
            rwlock.writeLock().unlock();
        }

        public List<String> getIps() {
            rwlock.readLock().lock();
            try {
                return ips;
            } finally {
                rwlock.readLock().unlock();
            }
        }

    }

}
