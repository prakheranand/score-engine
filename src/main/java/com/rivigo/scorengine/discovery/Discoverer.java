package com.rivigo.scorengine.discovery;

import com.rivigo.scorengine.commons.ConfigReader;
import com.rivigo.scorengine.commons.TargetServiceMetadata;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

public interface Discoverer {
    public List<String> discover(TargetServiceMetadata.TargetMetadata tm);
    public void scheduleDiscovery(TargetServiceMetadata.TargetMetadata tsm);
}
