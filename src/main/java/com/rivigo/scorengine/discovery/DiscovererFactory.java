package com.rivigo.scorengine.discovery;

import com.rivigo.scorengine.commons.ConfigReader;
import com.rivigo.scorengine.commons.TargetServiceMetadata;
import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import com.rivigo.scorengine.commons.TargetServiceMetadata.*;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class DiscovererFactory {
    private final ConfigReader configReader;
    private final ScheduledExecutorService scheduler;
    private final Discoverer consulDiscoverer;

    public Discoverer getDiscoverer(TargetMetadata targetMetadata){
        if(targetMetadata.getPlatform().equalsIgnoreCase(DiscoveryPlatform.consul.getValue())){
            return consulDiscoverer;
        }
        return null;
    }

    public void discoverAndSchedule() {
        log.info("inside discovery factory");
        TargetServiceMetadata tsm = configReader.getTargetServiceMetadata();
        Map<String, TargetMetadata> targetMetadataMap = tsm.getTarget();
        if(targetMetadataMap != null) {
            for(Map.Entry<String, TargetMetadata> entry : targetMetadataMap.entrySet()) {
                if(entry.getValue().getDiscovery() == 1) {
                    log.debug("discovery is on for service : {}", entry.getValue().getName());
                    Discoverer discoverer = getDiscoverer(entry.getValue());
                    entry.getValue().setIps(discoverer.discover(entry.getValue()));
                    discoverer.scheduleDiscovery(entry.getValue());
                }
            }
        }
    }

}
