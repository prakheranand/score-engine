package com.rivigo.scorengine.discovery;

import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.QueryParams;
import com.ecwid.consul.v1.Response;
import com.ecwid.consul.v1.health.model.Check;
import com.ecwid.consul.v1.health.model.HealthService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rivigo.scorengine.commons.ConfigReader;
import com.rivigo.scorengine.commons.TargetServiceMetadata;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
@Slf4j
public class ConsulDiscoverer implements Discoverer {
    private final ConfigReader configReader;
    private final ScheduledExecutorService scheduler;
    private final ConsulClient client;

    private static final String protocol="http://";
    @Getter
    private ConcurrentHashMap<String, List<String>> passingServices = new ConcurrentHashMap<>();
    @Getter
    private ConcurrentHashMap<String, List<String>> failingServices = new ConcurrentHashMap<>();
    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();


    @Override
    public List<String> discover(TargetServiceMetadata.TargetMetadata tsm) {
        getAllServicesNames(tsm);
        return getIps(tsm);
    }

    @Override
    public void scheduleDiscovery(TargetServiceMetadata.TargetMetadata tsm) {
        TargetServiceMetadata.GlobalConfig globalConfig = configReader.getTargetServiceMetadata().getGlobal();
        scheduler.scheduleWithFixedDelay(() -> getAndSet(tsm), globalConfig.getDiscoveryTtl(),
                globalConfig.getDiscoveryTtl(), TimeUnit.SECONDS );
        scheduler.scheduleWithFixedDelay(() -> getAllServicesNames(tsm), 60, 10, TimeUnit.SECONDS);
    }

    public void getAndSet(TargetServiceMetadata.TargetMetadata tm) {
        try {
            tm.setIps(getIps(tm));
        } catch (RuntimeException e){
            log.error("exception occurred while querying consul for service {}.\n {}",
                    tm.getName(), e);
        }
    }

    public List<String> getIps(TargetServiceMetadata.TargetMetadata tm) {
        List<String> ips = new ArrayList<>();
        for(String serviceName : passingServices.get(tm.getName())) {
            Response<List<HealthService>> healthyServices =
                    client.getHealthServices(serviceName, true, QueryParams.DEFAULT);
            for (HealthService healthService : healthyServices.getValue()) {
                HealthService.Service service = healthService.getService();
                if (service != null) {
                    ips.add(protocol + service.getAddress() + ":" + service.getPort() + tm.getMetricsUri());
                }
            }
        }
        return ips;
    }

   public void getAllServicesNames(TargetServiceMetadata.TargetMetadata tm) {
        log.info("querying for service {}" , tm.getName());
        List<String> passingList = new ArrayList<>();
        List<String> failingList = new ArrayList<>();

       try {
            Response<List<Check>> serviceCheckList = client.getHealthChecksState(null, null);
            for (Check check : serviceCheckList.getValue()) {
                if (check.getServiceName().toLowerCase().contains(tm.getName())) {
                    if (check.getStatus().equals(Check.CheckStatus.PASSING)) {
                        passingList.add(check.getServiceName());
                    } else {
                        failingList.add(check.getServiceName());
                    }
                }
            }
            log.info("passing instances {} \n, failing instances {}",
                    gson.toJson(passingList), gson.toJson(failingList));
            passingServices.put(tm.getName(), passingList);
            failingServices.put(tm.getName(), failingList);
        } catch (RuntimeException rte) {
            log.error("error while getting services list ", rte);
        }
    }
}
