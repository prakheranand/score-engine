package com.rivigo.scorengine.discovery;

import lombok.Getter;

public enum DiscoveryPlatform {
    consul("consul");

    @Getter
    private final String value;

    DiscoveryPlatform(String s) {
        value = s;
    }

    public DiscoveryPlatform getByValue(String s) {
        switch (s.toLowerCase()){
            case "consul" :
                return DiscoveryPlatform.consul;

        }
        return null;
    }
}
