package com.rivigo.scorengine.service.module;

import com.ecwid.consul.v1.ConsulClient;
import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.rivigo.scorengine.commons.ConfigReader;
import com.rivigo.scorengine.commons.MetricsScraper;
import com.rivigo.scorengine.discovery.ConsulDiscoverer;
import com.rivigo.scorengine.discovery.DiscovererFactory;
import com.rivigo.scorengine.service.ScoreServlet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration.Configuration;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ScoreModule extends AbstractModule{

    @Singleton
    @Provides
    public ScoreServlet providesScoreServlet(MetricsScraper metricsScraper){
        return new ScoreServlet(metricsScraper);
    }

    @Provides
    @Singleton
    public ConfigReader providesConfigReader() throws IOException {
        return new ConfigReader();
    }

    @Provides
    @Singleton
    public MetricsScraper providesMetricsScraper(ConfigReader configReader, HttpClient httpClient
            , ScheduledExecutorService scheduledExecutorService, DiscovererFactory ds) {
        ds.discoverAndSchedule();
        return new MetricsScraper(configReader, httpClient, scheduledExecutorService);
    }

    @Provides
    @Singleton
    public HttpClient providesHttpClient(ConfigReader configReader) {
        //TODO: below parmas should be in config
        Configuration config = configReader.getScoreConfig();
        int DEFAULT_MAX_CONNECTIONS_PER_HOST = 50;
        int DEFAULT_MAX_CONNECTIONS_TOTAL = 10000;
        int DEFAULT_CONNECTION_TIMEOUT = 1000;
        int DEFAULT_SOCKET_TIMEOUT = 10000;
        long DEFAULT_CLEANUP_FREQ = 5000l;
        long DEFAULT_IDLE_CONNECTION_IN_POOL_TIMEOUT = 60 * 1000;

        int maxConnectionPerHost =
                config.getInt("max-connection-per-host", DEFAULT_MAX_CONNECTIONS_PER_HOST);
        int maximumConnectionsTotal =
                config.getInt("max-connections-total", DEFAULT_MAX_CONNECTIONS_TOTAL);
        int connectionTimeoutMilliSecs =
                config.getInt("connection-timeout", DEFAULT_CONNECTION_TIMEOUT);
        int socketTimeoutMilliSecs = config.getInt("socket-timeout", DEFAULT_SOCKET_TIMEOUT);
        long cleanupThreadRunFrequency = config.getLong("cleanup-frequency", DEFAULT_CLEANUP_FREQ);
        long idleConnectionInPoolTimeoutInMilliSecs =
                config.getLong("idle-connection-in-pool-timeout",
                        DEFAULT_IDLE_CONNECTION_IN_POOL_TIMEOUT);
        final SocketConfig socketConfig = SocketConfig.custom()
                .setSoTimeout(socketTimeoutMilliSecs)
                .setSoKeepAlive(true)
                .build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setDefaultMaxPerRoute(maxConnectionPerHost);
        connectionManager.setMaxTotal(maximumConnectionsTotal);
        connectionManager.setDefaultSocketConfig(socketConfig);

        final RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(connectionTimeoutMilliSecs)
                .build();

        final HttpClientBuilder params = HttpClients.custom();
        params.setConnectionManager(connectionManager);
        params.setDefaultRequestConfig(requestConfig);

        HttpClient client = params.disableAutomaticRetries().build();
        Timer timer = new Timer("Http client reaper", true);
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                try {
                    connectionManager.closeExpiredConnections();
                } catch (Exception e) {
                    log.warn("Could not close expired connections", e);
                }
                try {
                    connectionManager.closeIdleConnections(idleConnectionInPoolTimeoutInMilliSecs,
                            TimeUnit.MILLISECONDS);
                } catch (Exception e) {
                    log.warn("Could not close Idle Connections", e);
                }
            }
        };
        timer.schedule(task, cleanupThreadRunFrequency, cleanupThreadRunFrequency);
        return client;
    }

    @Provides
    @Singleton
    public ScheduledExecutorService providesScheduledExecutorService(ConfigReader configReader){
        ScheduledExecutorService scheduler;
        scheduler = Executors.newScheduledThreadPool(configReader.getTargetServiceMetadata()
                .getGlobal().getSchedulerThread());
        return scheduler;
    }

    @Provides
    @Singleton
    public DiscovererFactory providesDiscovererFactory(ConfigReader configReader,ScheduledExecutorService
            scheduledExecutorService, ConsulDiscoverer consulDiscoverer){
        return new DiscovererFactory(configReader, scheduledExecutorService, consulDiscoverer);
    }

    @Provides
    @Singleton
    public ConsulDiscoverer providesConsulDiscoverer(ConfigReader configReader,
                                                     ScheduledExecutorService s, ConsulClient cs) {
        return new ConsulDiscoverer(configReader, s, cs);
    }

    @Provides
    @Singleton
    public ConsulClient providesConsulClient (ConfigReader configReader) {
        if(!configReader.getTargetServiceMetadata().getGlobal().getConsulIps().isEmpty()){
            String ipAndPort = configReader.getTargetServiceMetadata().
                    getGlobal().getConsulIps().get(0);
            String ip = ipAndPort.split(":")[0];
            int port = Integer.parseInt(ipAndPort.split(":")[1]);
            return new ConsulClient(ip, port);
        }
        return null;
    }


}
