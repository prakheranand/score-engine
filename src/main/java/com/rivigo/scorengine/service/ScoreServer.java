package com.rivigo.scorengine.service;

import com.google.inject.Injector;
import io.prometheus.client.exporter.MetricsServlet;
import io.prometheus.client.hotspot.DefaultExports;
import io.prometheus.client.jetty.JettyStatisticsCollector;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.StatisticsHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

@Slf4j
public class ScoreServer  {

    public static void main(String[] args) throws Exception{

        Injector injector = StaticInjector.getInjector();
        //TODO : through config
        Server server = new Server(4444);
        ServletContextHandler servletContextHandler = new ServletContextHandler
                (server, "/", true, false);
        server.setHandler(servletContextHandler);
        //adding score servlet
        ScoreServlet scoreServlet = injector.getInstance(ScoreServlet.class);
        servletContextHandler.addServlet(new ServletHolder(scoreServlet),"/score");

        //adding metrics servlet
        servletContextHandler.addServlet(new ServletHolder(new MetricsServlet()),"/metrics");
        // Add metrics about CPU, JVM memory etc.
        DefaultExports.initialize();

        // Configure StatisticsHandler.
        StatisticsHandler stats = new StatisticsHandler();
        stats.setHandler(server.getHandler());
        server.setHandler(stats);
        // Register collector.
        new JettyStatisticsCollector(stats).register();

        server.start();
        server.join();

    }

}
