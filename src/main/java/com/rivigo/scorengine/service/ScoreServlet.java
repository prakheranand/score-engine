package com.rivigo.scorengine.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rivigo.scorengine.commons.MetricsScraper;
import com.rivigo.scorengine.commons.TargetServiceMetadata;
import com.rivigo.scorengine.discovery.ConsulDiscoverer;
import com.rivigo.scorengine.exporter.Exporter;
import io.prometheus.client.Gauge;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Slf4j
public class ScoreServlet extends HttpServlet {
    private MetricsScraper metricsScraper;
    private Gson gson;


    public ScoreServlet(MetricsScraper metricsScraper){
        this.metricsScraper = metricsScraper;
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String service = request.getParameter("service");
        boolean detail = false;
        if(service == null) {
            service="";
        }
        else {
            String det;
            if(( det = request.getParameter("detail")) != null ){
                detail = Boolean.parseBoolean(det);
            }
        }
        Map<String, Gauge> metrics = metricsScraper.getGaugeMap();
        Map<String, Exporter> exporterMap = metricsScraper.getExporterMap();
        JsonObject jsonObject = new JsonObject();
        for(String key : metrics.keySet()) {
            if(key.toLowerCase().contains(service.toLowerCase())){
                if(detail) {
                    jsonObject.add(key, exporterMap.get(key).getJsonObject());
                }
                else {
                    jsonObject.addProperty(key, metrics.get(key).get());
                }
            }
        }

        response.getWriter().write(gson.toJson(jsonObject));
        response.getWriter().flush();
    }

}
