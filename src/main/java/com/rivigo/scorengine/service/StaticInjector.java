package com.rivigo.scorengine.service;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.rivigo.scorengine.service.module.ScoreModule;
import lombok.Getter;

public class StaticInjector {

    private StaticInjector(){}

    @Getter
    private static Injector injector;

    static {
        injector = Guice.createInjector(
                new ScoreModule()
        );
    }


}
