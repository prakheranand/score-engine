package com.rivigo.scorengine.exporter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rivigo.scorengine.commons.TargetServiceMetadata;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import static com.rivigo.scorengine.commons.SupportedMetrics.prometheusMetrics.*;
import static com.rivigo.scorengine.commons.TargetServiceMetadata.*;

@Slf4j
public class PrometheusExporter implements Exporter {
    //TODO: all critical and good value must come from configs

    private double loggingScore;
    private double dbaScore;
    private double responseTimeScore;
    private double responseCodeScore;
    private double cpuScore;
    private double memeoryScore;
    private double threadScore;
    @Setter
    private long lastUpdated;
    private transient Gson gson;

    public PrometheusExporter(HttpResponse response, TargetMetadata targetMetadata)
            throws IOException {
        gson = new GsonBuilder().setPrettyPrinting().create();
        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            List<String> lines = new ArrayList<>();
            InputStream is = response.getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(is));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if(line.startsWith("#")) {
                    continue;
                }
                lines.add(line);
            }
            log.trace("response from remote server :\n {} ", gson.toJson(lines));
            setLoggingScore(lines, targetMetadata);
            setDbaScore(lines, targetMetadata);
            setResponseTimeScore(lines, targetMetadata);
            setResponseCodeScore(lines, targetMetadata);
            setCpuScore(lines, targetMetadata);
            setMemeoryScore(lines, targetMetadata);
            setThreadScore(lines, targetMetadata);
            setLastUpdated(System.currentTimeMillis());

        }  else {
            log.warn("bad response - {} - form remote server", response.getStatusLine());
            throw new RuntimeException("bad response");
        }

    }

    public void setLoggingScore(List<String> lines, TargetMetadata tmd) {
        double errorLog = 0, warnlog =0;
        for(String line : lines){
            if(line.contains(logback_events_total.toString()) && line.contains("level=\"error\"")){
                String [] arr = line.split(" ");
                errorLog = Double.parseDouble(arr[arr.length -1 ]);
            }
            if(line.contains(logback_events_total.toString()) && line.contains("level=\"warn\"")){
                String [] arr = line.split(" ");
                warnlog = Double.parseDouble(arr[arr.length -1 ]);
            }
        }
        double scoreError = (errorLog - tmd.getMinErrorLog())/(tmd.getMaxErrorLog() -tmd.getMinErrorLog());
        double scorewarn = (warnlog - tmd.getMinWarnLog())/(tmd.getMaxWarnLog() -tmd.getMinWarnLog());
        double totalScore = (scoreError + scorewarn )/2.0;
        if(totalScore < 1) {
            loggingScore = 1;
        } else {
            loggingScore = 1 - (totalScore *5);
        }
    }

    @Override
    public double getLoggingScore() {
        return loggingScore;
    }

    public void setDbaScore(List<String> lines,TargetMetadata tmd) {
        double connection = 0;
        for(String line : lines){
            if(line.contains(data_source_active_connections.toString()) && line.contains("dataSource")){
                String [] arr = line.split(" ");
                connection = Double.parseDouble(arr[arr.length -1 ]);
                break;
            }
        }
        //TODO : think better algo
        if(connection >= tmd.getMinDbaConnection() && connection <= tmd.getMaxDbaConnection()){
            dbaScore = 1.0;
        }
        else {
            dbaScore = .5;
        }

    }

    @Override
    public double getdbsScore() {
        return dbaScore;
    }

    public void setResponseTimeScore(List<String> lines,TargetMetadata tmd) {

        double totalSum = 0, totalCount=0;
        String count = http_server_requests_duration_seconds + "_count";
        String sum = http_server_requests_duration_seconds + "_sum";
        for(String line : lines){
            if(line.contains(count)){
                String [] arr = line.split(" ");
                totalCount += Double.parseDouble(arr[arr.length -1 ]);
            }
            if(line.contains(sum)){
                String [] arr = line.split(" ");
                totalSum += Double.parseDouble(arr[arr.length -1 ]);
            }
        }
        double avgResponseTime =  totalSum/totalCount;
        //TODO : think better algo
        double score = (avgResponseTime - tmd.getMinReqLatency()) /
                (tmd.getMaxreqLatency() - tmd.getMinReqLatency());
        if(score > 1.0){
            responseTimeScore = 1.0;
        }
        else {
            responseTimeScore = 1 - (score * 5);
        }

    }

    @Override
    public double getResponseTimeScore() {
        return responseTimeScore;
    }

    public void setResponseCodeScore(List<String> lines,TargetMetadata tmd) {
        double nonOkCount = 0, totalCount=0;
        for(String line : lines){
            if(line.contains(http_server_requests_duration_seconds_count.toString())){
                String [] arr = line.split(" ");
                totalCount += Double.parseDouble(arr[arr.length -1 ]);
                if(!line.contains("status=\"200")){
                    nonOkCount += Double.parseDouble(arr[arr.length -1 ]);
                }
            }
        }
        double avgResponse =  (nonOkCount/totalCount)*100;
        //TODO : think better algo
        if(avgResponse < tmd.getMaxNonOk()){
            responseCodeScore = 1.0;
        }
        else {
            responseCodeScore = 1 - (((avgResponse - tmd.getMaxNonOk())/(tmd.getMaxNonOk())) * 5);
        }
    }

    @Override
    public double getResponseCodeScore() {
        return responseCodeScore;
    }

    public void setCpuScore(List<String> lines, TargetMetadata tmd) {
        double cpuUsages = 0;
        for(String line : lines){
            if(line.contains(system_load_average_1m.toString()) && !line.startsWith("#")){
                String [] arr = line.split(" ");
                cpuUsages = (Double.parseDouble(arr[arr.length -1 ])) *100;
                break;
            }
        }
        //TODO : think better algo
        if(cpuUsages < tmd.getMaxSysLoad()){
            cpuScore = 1.0;
        }
        else {
            cpuScore =  1- ((cpuUsages - tmd.getMaxSysLoad()) * 10 /(tmd.getMaxSysLoad() - tmd.getMinSysLoad()));

        }

    }

    @Override
    public double getCpuScore() {
        return cpuScore;
    }

    public void setMemeoryScore(List<String> lines, TargetMetadata tmd)  {
        String usedBytes = jvm_memory.toString() + "_used_bytes";
        String commitedBytes = jvm_memory.toString() +"_committed_bytes";
        double used = 0, committed=0;
        for(String line : lines){
            if(line.contains(usedBytes)){
                String [] arr = line.split(" ");
                used += Double.parseDouble(arr[arr.length -1 ]);
            }
            if(line.contains(commitedBytes)){
                String [] arr = line.split(" ");
                committed += Double.parseDouble(arr[arr.length -1 ]);
            }
        }
        double memoryUsages =  used/committed;
        //TODO : think better algo
        if(memoryUsages < 0.9){
            memeoryScore = 1.0;
        }
        else if(memoryUsages >= 1.0){
            memeoryScore = 0;
        } else {
            memeoryScore = memoryUsages/2;
        }
    }

    @Override
    public double getMemoryScore() {
        return memeoryScore;
    }

    public void setThreadScore(List<String> lines, TargetMetadata tmd)  {
        /*
        i do not see thread metrics in cms endpoints. This must be part of actuator metrics.
        Need to debug why thread metrics are not present in actuator. Till that
        setting thread score to 1.
         */
        threadScore = 1.0;
    }

    @Override
    public double getThreadScore() {
        return threadScore;
    }

    @Override
    public long getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public JsonObject getJsonObject() {
        return gson.toJsonTree(this).getAsJsonObject();
    }
}
