package com.rivigo.scorengine.exporter;

import com.google.gson.JsonObject;

import java.util.Map;

public interface Exporter {
    public double getLoggingScore();
    public double getdbsScore();
    public double getResponseTimeScore();
    public double getResponseCodeScore();
    public double getCpuScore();
    public double getMemoryScore();
    public double getThreadScore();
    public long getLastUpdated();
    public JsonObject getJsonObject();
}
